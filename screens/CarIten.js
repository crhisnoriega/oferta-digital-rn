import React, { Component } from "react";
import {
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  Dimensions,
  StatusBar,
  Image
} from "react-native";

import { Modal, TouchableHighlight, Alert } from "react-native";

import { TabView, SceneMap, TabBar } from "react-native-tab-view";

import { FontAwesome } from "@expo/vector-icons";
import { DrawerActions } from "@react-navigation/native";
import CartStatus from "../components/CartStatus";

import Configs from "../constants/Configs";
import axios from "axios";

//import firebase from "../infra/Firebase";

import FirebaseStore from "../infra/FirebaseStore";

class CarIten extends Component {
  state = {
    subTotal: 0,
    modalVisible: false,
    quantity: 1
  };

  constructor() {
    super();
    this.firebase = new FirebaseStore();
  }

  async componentDidMount() {
    this.calculateSubtotal();
  }

  doRemove = () => {
    var firebase_path =
      "users/" +
      this.props.username +
      "/cart/" +
      this.props.item.prodId.prod_id;

    console.log(firebase_path);
    this.firebase.removeFirebase(firebase_path);
  };

  calculateSubtotal = () => {
    var price = parseFloat(this.props.item.currentDePrice.value1);
    var subTotal = price * this.state.quantity;

    this.setState({ subTotal });
  };

  render() {
    var url =
      Configs.url_endoint +
      "/products/request_url_images?native_id=" +
      this.props.item.mediasList[0].nativeimage_id;
    console.log(url);
    return (
      <View style={[styles.itemContainer, { backgroundColor: "white" }]}>
        <View style={{ flexDirection: "row" }}>
          <Image
            style={{ width: 100, height: 100 }}
            source={{
              uri: url,
              method: "GET",
              headers: {
                "Content-type": "application/json",
                Authorization: "Bearer " + this.props.token
              }
            }}
          />
          <View>
            <Text
              style={{
                fontSize: 16,
                color: "black",
                fontWeight: "600",
               
                marginRight: 10,
                marginTop: 10
              }}
            >
              {this.props.item.name}
            </Text>

            <View style={{}}>
              <Text
                style={{
                  fontSize: 16,
                  color: "black",
                  fontWeight: "600",
                  
                  marginRight: 10
                }}
              >
                Quantidade: {this.props.buyQtde}
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  color: "black",
                  fontWeight: "600",
                  
                  marginRight: 10
                }}
              >
                Valor:{" "}
                {this.props.item.currentPrice.value1 * this.props.buyQtde}
              </Text>
            </View>
          </View>
        </View>
        <TouchableOpacity
          style={{ marginLeft: "auto", margin: 10 }}
          onPress={() => {
            this.doRemove();
          }}
        >
          <FontAwesome name="remove" size={30} color="red" />
        </TouchableOpacity>
      </View>
    );
  }
}

export default CarIten;

const styles = StyleSheet.create({
  gridView: {
    marginTop: 20,
    flex: 1
  },
  itemContainer: {
    marginTop: 5,

    borderRadius: 5
  },
  itemName: {
    fontSize: 16,
    color: "black",
    fontWeight: "600"
  },
  itemCode: {
    fontWeight: "600",
    fontSize: 12,
    color: "#fff"
  }
});
