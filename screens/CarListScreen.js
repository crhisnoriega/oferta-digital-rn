import React, { Component } from "react";
import {
  TouchableOpacity,
  Text,
  View,
  ScrollView,
  StyleSheet,
  Dimensions,
  StatusBar,
  Picker
} from "react-native";
import { Card, ListItem, Icon } from "react-native-elements";

import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { MaterialIcons } from "@expo/vector-icons";
import { DrawerActions } from "@react-navigation/native";
import BuyStatus from "../components/BuyStatus";
import { FlatGrid } from "react-native-super-grid";

import CarIten from "./CarIten";
import Configs from "../constants/Configs";

// import firebase from "../infra/Firebase";
import FirebaseStore from "../infra/FirebaseStore";

import { AsyncStorage } from "react-native";

import { Modal, TouchableHighlight, Alert } from "react-native";

import axios from "axios";

class CarListScreen extends Component {
  state = {
    carItens: []
  };
  constructor() {
    super();
    this.setState({ products: [] });
    this.firebase = new FirebaseStore();
  }
  async componentDidMount() {
    var username = await AsyncStorage.getItem("username");
    var token = await AsyncStorage.getItem("token");

    this.firebase.retrieveFirebase("users/" + username + "/cart", data => {
      var carItens = [];
      var total = 0;
      var cart_val = data;
      var keys = Object.keys(cart_val);

      for (var i = 0; i < keys.length; i++) {
        var item = cart_val[keys[i]];
        total += item.item.currentPrice.value1 * item.buyQtde;
        carItens.push(item);
      }

      this.setState({
        carItens,
        total,
        username,
        token
      });
    });
  }

  render() {
    var _token = this.state.token;
    var _username = this.state.username;

    return (
      <View style={{ flex: 1 }}>
        <StatusBar barStyle="light-content" />
        <BuyStatus
          showsearch={false}
          navigation={this.props.navigation}
          style={{ marginTop: 0 }}
        />

        <ScrollView style={{ flex: 1 }}>
          <Card containerStyle={{ backgroundColor: "#000000" }}>
            <Text style={{ color: "white", fontSize: 20 }}>Entrega</Text>
            <Picker
              style={{ height: 30, width: 400 }}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ language: itemValue })
              }
            >
              <Picker.Item label="Java" value="java" />
              <Picker.Item label="JavaScript" value="js" />
            </Picker>
          </Card>

          <Card containerStyle={{ backgroundColor: "#000000" }}>
            <Text style={{ color: "white", fontSize: 20 }}>
              Total: {this.state.total}
            </Text>
          </Card>
          <FlatGrid
            style={{ backgroundColor: "#ff000000" }}
            onEndReached={() => {}}
            onEndReachedThreshold={0.7}
            itemDimension={Dimensions.get("window").width}
            items={this.state.carItens}
            renderItem={({ item, index }) => (
              <CarIten
                navigation={this.props.navigation}
                callback={this.props.callback}
                token={_token}
                username={_username}
                item={item.item}
                buyQtde={item.buyQtde}
              />
            )}
          />

          <TouchableOpacity style={{ marginBottom: 20 }} onPress={() => {}}>
            <Card containerStyle={{ backgroundColor: "blue" }}>
              <Text
                style={{ textAlign: "center", color: "white", fontSize: 20 }}
              >
                FINALIZAR COMPRA
              </Text>
            </Card>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

export default CarListScreen;

const styles = StyleSheet.create({
  gridView: {
    marginTop: 20,
    flex: 1
  },
  itemContainer: {
    justifyContent: "flex-end",
    borderRadius: 5,
    padding: 10,
    height: 150
  },
  itemName: {
    fontSize: 16,
    color: "#fff",
    fontWeight: "600"
  },
  itemCode: {
    fontWeight: "600",
    fontSize: 12,
    color: "#fff"
  }
});
