import React, { Component } from "react";
import {
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  Dimensions,
  StatusBar,
  Image
} from "react-native";

import { Modal, TouchableHighlight, Alert } from "react-native";

import { TabView, SceneMap, TabBar } from "react-native-tab-view";

import { FontAwesome } from "@expo/vector-icons";
import { DrawerActions } from "@react-navigation/native";
import CartStatus from "../components/CartStatus";

import Configs from "../constants/Configs";
import axios from "axios";

//import firebase from "../infra/Firebase";
import FirebaseStore from "../infra/FirebaseStore";

class ProductDetails extends Component {
  state = {
    subTotal: 0,
    modalVisible: false,
    quantity: 1
  };

  constructor() {
    super();
    this.firebase = new FirebaseStore();
  }

  async componentDidMount() {
    this.calculateSubtotal();
  }

  doBuy = () => {
    var firebase_path =
      "users/" +
      this.props.username +
      "/cart/" +
      this.props.item.prodId.prod_id;

    console.log(firebase_path);
    this.firebase.persistFirebase(firebase_path, {
      item: this.props.item,
      buyQtde: this.state.quantity
    });

    this.setState({ modalVisible: false });
  };

  calculateSubtotal = () => {
    var price = parseFloat(this.props.item.currentDePrice.value1);
    var subTotal = price * this.state.quantity;

    this.setState({ subTotal });
  };

  render() {
    var url =
      Configs.url_endoint +
      "/products/request_url_images?native_id=" +
      this.props.item.mediasList[0].nativeimage_id;
    console.log(url);
    return (
      <View style={[styles.itemContainer, { backgroundColor: "white" }]}>
        <Image
          style={{ width: 200, height: 200, marginLeft: 60 }}
          source={{
            uri: url,
            method: "GET",
            headers: {
              "Content-type": "application/json",
              Authorization: "Bearer " + this.props.token
            }
          }}
        />

        <Text
          style={{
            fontSize: 16,
            color: "black",
            fontWeight: "600",
            marginLeft: 10,
            marginRight: 10
          }}
        >
          {this.props.item.name}
        </Text>

        <View style={{ flexDirection: "row" }}>
          <Text
            style={{
              fontSize: 16,
              color: "black",
              fontWeight: "600",
              marginLeft: 10,
              marginRight: 10
            }}
          >
            Preço: {this.props.item.currentPrice.value1}
          </Text>
          <TouchableOpacity
            style={{ marginLeft: "auto", margin: 10 }}
            onPress={() => {
              this.setState({ modalVisible: true });
            }}
          >
            <FontAwesome name="shopping-cart" size={30} color="red" />
          </TouchableOpacity>
        </View>
        {this.props.item.currentDePrice ? (
          <View
            style={{
              backgroundColor: "grey",
              width: "100%",
              height: 30,
              borderBottomLeftRadius: 5,
              borderBottomRightRadius: 5
            }}
          >
            <Text
              style={{
                marginLeft: 10,
                marginTop: 5,
                justifyContent: "center",
                color: "white",
                fontWeight: "bold"
              }}
            >
              Desconto: {this.props.item.currentDePrice.value1}
            </Text>
          </View>
        ) : (
          <View />
        )}

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            // Alert.alert("Modal has been closed.");
          }}
        >
          <View
            style={{
              flex: 1,
              borderRadius: 15,
              marginTop: 85,
              height: "100%",
              backgroundColor: "white",
              alignItems: "center",
              justifyContent: "bottom"
            }}
          >
            <Text
              style={{
                marginTop: 30,
                marginLeft: 20,
                marginRight: 20,
                fontSize: 14,
                fontWeight: "bold",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              {this.props.item.name}
            </Text>
            <Image
              style={{
                marginTop: 30,
                width: 200,
                height: 200,
                justifyContent: "center"
              }}
              source={{
                uri: url,
                method: "GET",
                headers: {
                  "Content-type": "application/json",
                  Authorization: "Bearer " + this.props.token
                }
              }}
            />
            <Text
              style={{
                fontSize: 20,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              {this.props.item.description}
            </Text>

            <View
              style={{
                marginTop: 50,
                flex: 1,
                flexDirection: "row",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  if (this.state.quantity > 1) {
                    this.setState({ quantity: this.state.quantity - 1 }, () => {
                      this.calculateSubtotal();
                    });
                  } else {
                    this.setState({ modalVisible: false });
                  }
                }}
              >
                <FontAwesome
                  name={this.state.quantity <= 1 ? "trash-o" : "minus"}
                  size={35}
                  color="black"
                />
              </TouchableOpacity>

              <Text
                style={{
                  fontSize: 35,
                  color: "black",
                  fontWeight: "bold",
                  marginLeft: 10,
                  marginTop: -2,
                  marginRight: 10
                }}
              >
                {this.state.quantity}
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ quantity: this.state.quantity + 1 }, () => {
                    this.calculateSubtotal();
                  });
                }}
              >
                <FontAwesome name="plus" size={35} color="black" />
              </TouchableOpacity>
            </View>

            <TouchableOpacity
              onPress={() => {
                this.doBuy();
              }}
              style={{
                height: 40,
                width: "80%",
                marginTop: 20,
                marginLeft: 30,
                marginBottom: 80,
                marginRight: 30,
                backgroundColor: "red",
                justifyContent: "center"
              }}
            >
              {this.state.refresh ? (
                <ActivityIndicator size="small" color="#ffec00" />
              ) : (
                <Text
                  style={{
                    color: "white",
                    fontSize: 16,
                    textAlign: "center"
                  }}
                >
                  Adicionar R${this.state.subTotal}
                </Text>
              )}
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

export default ProductDetails;

const styles = StyleSheet.create({
  gridView: {
    marginTop: 20,
    flex: 1
  },
  itemContainer: {
    justifyContent: "flex-end",
    borderRadius: 5
  },
  itemName: {
    fontSize: 16,
    color: "black",
    fontWeight: "600"
  },
  itemCode: {
    fontWeight: "600",
    fontSize: 12,
    color: "#fff"
  }
});
