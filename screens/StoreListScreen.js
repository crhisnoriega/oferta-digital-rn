import React, { Component } from "react";
import {
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  Dimensions,
  StatusBar,
  AsyncStorage
} from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { MaterialIcons } from "@expo/vector-icons";
import { DrawerActions } from "@react-navigation/native";
import CartStatus from "../components/CartStatus";
import { FlatGrid } from "react-native-super-grid";
import { Card, ListItem, Icon } from "react-native-elements";
import axios from "axios";

import Configs from "../constants/Configs";

class StoreListScreen extends Component {
  state = {
    stores: []
  };
  constructor() {
    super();
  }

  goApp = async store => {
    await AsyncStorage.setItem("store", JSON.stringify(store));
    this.props.navigation.navigate("App");
  };

  async componentDidMount() {
    this.token = await AsyncStorage.getItem("token");
    axios
      .get(
        Configs.url_endoint + "/users/findstores",

        {
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: "Bearer " + this.token
          }
        }
      )
      .then(result => {
        console.log(result.data);
        this.setState({ stores: result.data });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <View style={{ backgroundColor: "#ffffff", flex: 1 }}>
        <StatusBar barStyle="dark-content" />
        <FlatGrid
          style={{ backgroundColor: "#ff000000", marginTop: 50 }}
          onEndReached={() =>
            console.log("called from list" + this.props.category)
          }
          onEndReachedThreshold={0.7}
          itemDimension={Dimensions.get("window").width}
          items={this.state.stores}
          // staticDimension={300}
          // fixed
          // spacing={20}
          renderItem={({ item, index }) => (
            <TouchableOpacity
              onPress={() => {
                this.goApp(item);
              }}
            >
              <Card containerStyle={{ backgroundColor: "#000000" }}>
                <Text style={{ color: "white", fontSize: 20 }}>
                  {item.nomeFantasia}
                </Text>
                <Text style={{ color: "white", marginTop: 10 }}>
                  {item.description}
                </Text>
                <Text style={{ color: "white" }}>
                  {item.address.xlgr}
                  {item.address.xbairro}
                </Text>
              </Card>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}

export default StoreListScreen;

const styles = StyleSheet.create({
  gridView: {
    marginTop: 20,
    flex: 1
  },
  itemContainer: {
    justifyContent: "flex-end",
    borderRadius: 5,
    padding: 10,
    height: 150
  },
  itemName: {
    fontSize: 16,
    color: "#fff",
    fontWeight: "600"
  },
  itemCode: {
    fontWeight: "600",
    fontSize: 12,
    color: "#fff"
  }
});
