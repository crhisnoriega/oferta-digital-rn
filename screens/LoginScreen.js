import React, { Component } from "react";
import {
  View,
  Button,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Image,
  Alert,
  Linking,
  ImageBackground,
  Platform,
  Modal
} from "react-native";
import { AsyncStorage } from "react-native";
import { KeyboardAvoidingView } from "react-native";
import { checkInternetConnection } from "react-native-offline";
import { ActivityIndicator } from "react-native-paper";
import { Ionicons } from "@expo/vector-icons";
import { FontAwesome } from "@expo/vector-icons";
import { MaterialIcons } from "@expo/vector-icons";
import { Card, ListItem, Icon } from "react-native-elements";
import axios from "axios";

import Configs from "../constants/Configs";

const foregroundColor1 = "gray";
const foregroundColor2 = "black";

const users = [
  {
    name: "brynn",
    avatar: "https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg"
  }
];

class Login extends Component {
  state = {
    emailS: "",
    email: "",
    login: "",
    senha: "",
    cancelamento: 0,
    idUsuario: "",
    modalVisibleSenha: false
  };

  static navigationOptions = {
    drawerLockMode: "locked-closed"
  };

  async componentDidMount() {}

  login = async () => {
    console.log("test");
    var url = Configs.url_endoint + "/users/autenthication";
    console.log(url);
    axios
      .post(
        url,
        { username: "app2", password: "app2", store_id: "serradalto" },
        {
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
          }
        }
      )
      .then(result => {
        console.log(result.data);
        if (result.data.token) {
          AsyncStorage.setItem("token", result.data.token);
          AsyncStorage.setItem("user", JSON.stringify(result.data));
          AsyncStorage.setItem("username", result.data.username);
          this.props.navigation.navigate("StoreListScreen");
        } else {
          Alert.alert("Login", result.data.message);
        }
      })
      .catch(error => {
        console.log(JSON.stringify(error));
        Alert.alert("Login", "Usuario ou senha invalidos");
      });
  };

  /* tela principal */
  render() {
    return (
      <ImageBackground
        style={{
          flex: 1,
          resizeMode: "cover",
          backgroundColor: "#ff000000",
          justifyContent: "flex-start"
        }}
      >
        <StatusBar
          barStyle="dark-content"
          translucent={true}
          backgroundColor={"transparent"}
        />

        <Card
          title="ADVERTENCIA"
          titleStyle={{ color: "white" }}
          containerStyle={{ marginTop: 50, backgroundColor: "black" }}
        >
          <View>
            <Image style={styles.image} resizeMode="cover" />
            <Text style={{ color: "white" }}>
              ALGUM TEXTO SOBRE AS COMPRAS PELO APLICATIVO
            </Text>
          </View>
        </Card>

        <Card containerStyle={{ marginTop: 100 }}>
          <KeyboardAvoidingView behavior="padding" enabled>
            <View
              style={{
                flexDirection: "row",
                marginTop: 60,
                marginLeft: 10,
                marginRight: 10,
                borderBottomWidth: 0.7,
                borderBottomColor: foregroundColor1,
                marginBottom: 10
              }}
            >
              <FontAwesome name="user" size={25} />
              <TextInput
                value={this.state.login}
                onChangeText={login => this.setState({ login })}
                keyboardType="email-address"
                underlineColorAndroid="transparent"
                placeholder="Email"
                placeholderTextColor="#c5c5c5"
                selectionColor={"black"}
                style={{
                  flex: 1,
                  height: 30,
                  backgroundColor: "transparent",
                  marginLeft: 10,
                  fontSize: 18,
                  color: "black",
                  fontWeight: "bold"
                }}
              />
            </View>

            <View
              style={{
                flexDirection: "row",
                marginLeft: 10,
                marginTop: 10,
                marginRight: 10,
                borderBottomWidth: 0.7,
                borderBottomColor: foregroundColor1,
                marginBottom: 10
              }}
            >
              <Ionicons name="md-lock" size={25} />
              <TextInput
                value={this.state.senha}
                onChangeText={senha => this.setState({ senha })}
                underlineColorAndroid="transparent"
                placeholder="Senha"
                placeholderTextColor="#c5c5c5"
                secureTextEntry={true}
                selectionColor={"black"}
                style={{
                  flex: 1,
                  height: 30,
                  backgroundColor: "transparent",
                  marginLeft: 10,
                  fontSize: 18,
                  color: "black",
                  fontWeight: "bold"
                }}
              />
            </View>
            <TouchableOpacity
              disabled={this.state.refresh}
              onPress={() => this.login()}
              style={{
                height: 40,

                marginTop: 20,
                paddingHorizontal: 10,
                justifyContent: "center",
                paddingVertical: 5,
                margin: 30,
                backgroundColor: "black"
              }}
            >
              {this.state.refresh ? (
                <ActivityIndicator size="small" color="#ffec00" />
              ) : (
                <Text
                  style={{
                    color: "white",
                    fontSize: 16,
                    textAlign: "center"
                  }}
                >
                  Entrar
                </Text>
              )}
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </Card>

        <TouchableOpacity
          disabled={this.state.refresh}
          onPress={() => {
            Linking.openURL("http://entremares.tur.br/associe-se/");
          }}
        >
          <Text
            style={{
              textAlign: "center",
              fontWeight: "bold",
              fontSize: 16,
              marginBottom: 40,
              marginTop: 20,
              color: foregroundColor2
            }}
          >
            REGISTRAR
          </Text>
        </TouchableOpacity>
      </ImageBackground>
    );
  }
}

export default Login;

const styles = StyleSheet.create({
  searchSection: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  searchIcon: {
    padding: 10
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: "#fff",
    color: "#424242"
  }
});
