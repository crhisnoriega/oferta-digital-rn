import React, { Component } from "react";
import {
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  Dimensions,
  StatusBar,
  ActivityIndicator,
  AsyncStorage
} from "react-native";

import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { MaterialIcons } from "@expo/vector-icons";
import { DrawerActions } from "@react-navigation/native";
import CartStatus from "../components/CartStatus";
import ProductListScreen from "./ProductListScreen";
import Configs from "../constants/Configs";

const initialLayout = { width: Dimensions.get("window").width };

const createTab = (category, callback, navigation) => (
  <ProductListScreen
    navigation={navigation}
    callback={callback}
    category={category}
  />
);

var quantity = 0;

class CategoriesScreen extends Component {
  state = {
    index: 0,
    routes: Configs.categories
  };

  async componentDidMount() {
    var category_select = await AsyncStorage.getItem("category_select");
    if (category_select == "MEDICAMENTOS") {
      this.setState({ index: 0 });
    }
    if (category_select == "DIAGNOSTICOS") {
      this.setState({ index: 1 });
    }
    if (category_select == "MAQUIAGEM") {
      this.setState({ index: 2 });
    }

    if (category_select == "CABELOS") {
      this.setState({ index: 3 });
    }

    if (category_select == "PES E MAOS") {
      this.setState({ index: 4 });
    }
    if (category_select == "DERMO") {
      this.setState({ index: 5 });
    }

    if (category_select == "HIGIENE") {
      this.setState({ index: 6 });
    }
    if (category_select == "INFANTIL") {
      this.setState({ index: 7 });
    }
  }

  renderScene2 = ({ route }) => {
    return (
      <ProductListScreen
        navigation={this.props.navigation}
        category={route.key}
        style={[styles.scene]}
      />
    );
  };

  buyOperation = item => {
    console.log("buy op");
    quantity = quantity + 1;
    this.cart.updateQ(quantity, item);
  };

  _handleIndexChange = index => this.setState({ index });

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar barStyle="light-content" />
        <CartStatus
          ref={ref => (this.cart = ref)}
          quantity={quantity}
          showsearch={true}
          navigation={this.props.navigation}
          style={{ marginTop: 0 }}
        />
        <TabView
          ref={ref => (this.tab = ref)}
          lazy={true}
          renderLazyPlaceholder={() => (
            <Text style={{ color: "red" }}>Carregando...</Text>
          )}
          renderTabBar={props => (
            <TabBar
              tabStyle={{ width: 150 }}
              scrollEnabled={true}
              style={{ backgroundColor: "black" }}
              indicatorStyle={{
                backgroundColor: "white",
                height: 5,
                marginBottom: 0
              }}
              {...props}
            />
          )}
          navigationState={this.state}
          renderScene={SceneMap({
            medicamentos: () =>
              createTab(
                "medicamentos",
                this.buyOperation,
                this.props.navigation
              ),
            diagnosticos: () =>
              createTab(
                "diagnosticos",
                this.buyOperation,
                this.props.navigation
              ),
            maquiagem: () =>
              createTab("maquiagem", this.buyOperation, this.props.navigation),
            cabelos: () =>
              createTab("cabelos", this.buyOperation, this.props.navigation),
            pes_maos: () =>
              createTab("pes_maos", this.buyOperation, this.props.navigation),
            dermo: () =>
              createTab("dermo", this.buyOperation, this.props.navigation),
            higiene: () =>
              createTab("higiene", this.buyOperation, this.props.navigation),
            infantil: () =>
              createTab("infantil", this.buyOperation, this.props.navigation)
          })}
          onIndexChange={this._handleIndexChange}
          initialLayout={initialLayout}
          index={2}
        />
      </View>
    );
  }
}

export default CategoriesScreen;

const styles = StyleSheet.create({
  scene: {
    flex: 1
  }
});
