import React, { Component } from "react";
import {
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  Dimensions,
  StatusBar,
  AsyncStorage
} from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { MaterialIcons } from "@expo/vector-icons";
import { DrawerActions } from "@react-navigation/native";
import CartStatus from "../components/CartStatus";
import { FlatGrid } from "react-native-super-grid";
import { Card, ListItem, Icon } from "react-native-elements";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Configs from "../constants/Configs";
import CategoriesScreen from "./CategoriesScreen";
import CategoriesMenu from "./CategoriesMenu";
import ProductToBuy from "./ProductToBuy";
import CarListScreen from "./CarListScreen";

const Stack = createStackNavigator();

class MyStack extends Component {
  state = {
    stores: []
  };
  constructor() {
    super();
  }

  async componentDidMount() {}

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "#fff"
        }}
      >
        <Stack.Navigator
          screenOptions={{
            headerShown: false
          }}
          navigationOptions={{
            gesturesEnabled: false
          }}
        >
          <Stack.Screen name="CategoriesMenu" component={CategoriesMenu} />
          <Stack.Screen name="CategoriesScreen" component={CategoriesScreen} />
          <Stack.Screen name="ProductToBuy" component={ProductToBuy} />
          <Stack.Screen name="CarListScreen" component={CarListScreen} />
          
        </Stack.Navigator>
      </View>
    );
  }
}

export default MyStack;

const styles = StyleSheet.create({
  gridView: {
    marginTop: 20,
    flex: 1
  },
  itemContainer: {
    justifyContent: "flex-end",
    borderRadius: 5,
    padding: 10,
    height: 150
  },
  itemName: {
    fontSize: 16,
    color: "#fff",
    fontWeight: "600"
  },
  itemCode: {
    fontWeight: "600",
    fontSize: 12,
    color: "#fff"
  }
});
