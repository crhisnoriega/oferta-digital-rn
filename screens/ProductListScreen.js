import React, { Component } from "react";
import {
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  Dimensions,
  StatusBar
} from "react-native";
import { AsyncStorage } from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { MaterialIcons } from "@expo/vector-icons";
import { DrawerActions } from "@react-navigation/native";
import CartStatus from "../components/CartStatus";
import { FlatGrid } from "react-native-super-grid";

import ProductDetails from "./ProductDetails";
import Configs from "../constants/Configs";

import axios from "axios";

const items = [
  { name: "TURQUOISE", code: "#1abc9c" },
  { name: "EMERALD", code: "#2ecc71" },
  { name: "PETER RIVER", code: "#3498db" },
  { name: "AMETHYST", code: "#9b59b6" },
  { name: "WET ASPHALT", code: "#34495e" },
  { name: "GREEN SEA", code: "#16a085" },
  { name: "NEPHRITIS", code: "#27ae60" },
  { name: "BELIZE HOLE", code: "#2980b9" },
  { name: "WISTERIA", code: "#8e44ad" },
  { name: "MIDNIGHT BLUE", code: "#2c3e50" }
];

class ProductListScreen extends Component {
  state = {
    products: [],
    token: null,
    username: null
  };
  constructor() {
    super();
    this.setState({ products: [] });
  }
  async componentDidMount() {
    this.token = await AsyncStorage.getItem("token");
    this.username = await AsyncStorage.getItem("username");
    var url = Configs.url_endoint + "/products/search_post";
    axios
      .post(
        url,
        {
          store_id: "store2e8c97a8-e91e-4186-ad4a-49cc4fc164ea",
          productName: "a",
          lowPrice: 0,
          upperPrice: 30000,
          categories: [this.props.category],
          pagination: { maxSize: 10, startWith: 0, calculateTotal: "true" }
        },
        {
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: "Bearer " + this.token
          }
        }
      )
      .then(result => {
        this.setState({
          products: result.data.products,
          token: this.token,
          username: this.username
        });
      })
      .catch(error => {
        console.log(JSON.stringify(error));
      });
  }

  render() {
    var _token = this.state.token;
    var _username = this.state.username;
    return (
      <FlatGrid
        style={{ backgroundColor: "#ff000000" }}
        onEndReached={() => {}}
        onEndReachedThreshold={0.7}
        itemDimension={Dimensions.get("window").width}
        items={this.state.products}
        renderItem={({ item, index }) => (
          <ProductDetails
            navigation={this.props.navigation}
            callback={this.props.callback}
            token={_token}
            username={_username}
            item={item}
          />
        )}
      />
    );
  }
}

export default ProductListScreen;

const styles = StyleSheet.create({
  gridView: {
    marginTop: 20,
    flex: 1
  },
  itemContainer: {
    justifyContent: "flex-end",
    borderRadius: 5,
    padding: 10,
    height: 150
  },
  itemName: {
    fontSize: 16,
    color: "#fff",
    fontWeight: "600"
  },
  itemCode: {
    fontWeight: "600",
    fontSize: 12,
    color: "#fff"
  }
});
