import React, { Component } from "react";
import {
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  Dimensions,
  StatusBar,
  Image
} from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";

import { FontAwesome } from "@expo/vector-icons";
import { DrawerActions } from "@react-navigation/native";
import CartStatus from "../components/CartStatus";

import Configs from "../constants/Configs";
import axios from "axios";

import firebase from "../infra/Firebase";

class ProductToBuy extends Component {
  constructor() {
    super();
  }

  render() {
    var url =
      Configs.url_endoint +
      "/products/request_url_images?native_id=" +
      this.props.route.params.item.mediasList[0].nativeimage_id;
    console.log(url);
    return (
      <View style={{ backgroundColor: "rgba(52, 52, 52, 0.8)" }}>
        <StatusBar
          barStyle="dark-content"
          translucent={true}
          backgroundColor={"transparent"}
        />
        <View
          style={{ backgroundColor: "red", marginTop: 80, borderRadius: 10 }}
        >
          <Image
            style={{ width: 200, height: 200, marginLeft: 60 }}
            source={{
              uri: url,
              method: "GET",
              headers: {
                "Content-type": "application/json",
                Authorization: "Bearer " + this.props.token
              }
            }}
          />

          <Text
            style={{
              fontSize: 16,
              color: "black",
              fontWeight: "600",
              marginLeft: 10,
              marginRight: 10
            }}
          >
            {this.props.route.params.item.name}
          </Text>

          <View style={{ flexDirection: "row", backgroundColor: "green" }}>
            <Text
              style={{
                fontSize: 16,
                color: "black",
                fontWeight: "600",
                marginLeft: 10,
                marginRight: 10
              }}
            ></Text>
            <TouchableOpacity
              style={{ marginLeft: "auto", margin: 10 }}
              onPress={() => {
                firebase
                  .database()
                  .ref("users")
                  .set(this.props.item);
              }}
            >
              <FontAwesome name="shopping-cart" size={30} color="black" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default ProductToBuy;

const styles = StyleSheet.create({
  gridView: {
    marginTop: 20,
    flex: 1
  },
  itemContainer: {
    justifyContent: "flex-end"
  },
  itemName: {
    fontSize: 16,
    color: "black",
    fontWeight: "600"
  },
  itemCode: {
    fontWeight: "600",
    fontSize: 12,
    color: "#fff"
  }
});
