import React, { Component } from "react";
import {
  TouchableWithoutFeedback,
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  Dimensions,
  StatusBar,
  Image
} from "react-native";
import { AsyncStorage } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import { DrawerActions } from "@react-navigation/native";
import { FlatGrid } from "react-native-super-grid";
import Configs from "../constants/Configs";
import { Card, ListItem, Icon } from "react-native-elements";

import CartStatus from "../components/CartStatus";

const items = Configs.categories;
class CategoriesMenu extends Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#ff000000" }}>
        <StatusBar barStyle="light-content" />
        <CartStatus
          showsearch={true}
          navigation={this.props.navigation}
          style={{ marginTop: 0 }}
        />
        <FlatGrid
          onEndReached={console.log("called")}
          itemDimension={130}
          items={items}
          // staticDimension={300}
          // fixed
          // spacing={20}
          renderItem={({ item, index }) => (
            <TouchableOpacity
              style={[styles.itemContainer, { backgroundColor: "#34495e" }]}
              delayPressIn={80}
              onPress={async () => {
                await AsyncStorage.setItem("category_select", item.name);
                this.props.navigation.navigate("CategoriesScreen");
              }}
            >
              <View style={[styles.itemContainer]}>
                <Image
                  style={{
                    width: 60,
                    height: 60,
                    tintColor: "white"
                  }}
                  source={require("../assets/images/menu_produtos.png")}
                  size={20}
                />
                <Text style={styles.itemName}>{item.name}</Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}

export default CategoriesMenu;

const styles = StyleSheet.create({
  gridView: {
    marginTop: 20,
    flex: 1
  },
  itemContainer: {
    justifyContent: "flex-end",
    borderRadius: 5,
    padding: 10,
    height: 150
  },
  itemName: {
    fontSize: 16,
    color: "#fff",
    fontWeight: "600"
  },
  itemCode: {
    fontWeight: "600",
    fontSize: 12,
    color: "#fff"
  }
});
