export default {
  url_endoint: "http://200.241.193.141/ofertadigitalrest",
  categories: [
    {
      key: "medicamentos",
      title: "MEDICAMENTOS",
      name: "MEDICAMENTOS",
      code: "#1abc9c"
    },
    {
      key: "diagnosticos",
      title: "DIAGNOSTICOS",
      name: "DIAGNOSTICOS",
      code: "#2ecc71"
    },
    {
      key: "maquiagem",
      title: "MAQUIAGEM",
      name: "MAQUIAGEM",
      code: "#3498db"
    },
    { key: "cabelos", title: "CABELOS", name: "CABELOS", code: "#9b59b6" },
    { key: "pes_maos", title: "PES MAOS", name: "PES E MAOS", code: "#34495e" },
    { key: "dermo", title: "DERMO", name: "DERMO", code: "#16a085" },
    { key: "higiene", title: "HIGIENE", name: "HIGIENE", code: "#27ae60" },
    { key: "infantil", title: "INFANTIL", name: "INFANTIL", code: "#2980b9" }
  ]
};
