import React, { Component } from "react";

import { createDrawerNavigator } from "@react-navigation/drawer";
import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem
} from "@react-navigation/drawer";
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  AsyncStorage
} from "react-native";

import { FontAwesome } from "@expo/vector-icons";
import BottomTabNavigator from "./BottomTabNavigator";

import { Avatar, Card } from "react-native-elements";

import MenuItem from "../components/MenuItem";
import MenuItemIcon from "../components/MenuItemIcon";
import CategoriesScreen from "../screens/CategoriesScreen";


const Drawer = createDrawerNavigator();

class CustomDrawerContent extends Component {
  state = {
    store: {}
  };

  async componentDidMount() {
    var json = await AsyncStorage.getItem("store");
    console.log(json);
    console.log("###########$$$$$$$$$%%%%%%%");
    this.setState({ store: JSON.parse(json) });
  }

  render() {
    return (
      <View style={{ flex: 1, marginTop: 0, backgroundColor: "black" }}>
        {/*Top Large Image */}
        {this.state.store ? (
          <Card
            containerStyle={{
              backgroundColor: "#000000",
              marginTop: 50,
              marginBottom: 30,
              justifyContent: "center"
            }}
          >
            <Text style={{ color: "white", fontSize: 20 }}>
              {this.state.store.nomeFantasia}
            </Text>
            <Text style={{ color: "white", marginTop: 10 }}>
              {this.state.store.description}
            </Text>
          </Card>
        ) : (
          <View />
        )}

      </View>
    );
  }
}

export default function DrawerNavigator({ navigation, route }) {
  return (
    <Drawer.Navigator
      style={{ marginTop: 80, backgroundColor: "black" }}
      drawerContent={props => <CustomDrawerContent {...props} />}
      initialRouteName="Home"
    >
      <Drawer.Screen name="BottomTabNavigator" component={BottomTabNavigator} />
    </Drawer.Navigator>
  );
}
