import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import * as React from "react";
import { View } from "react-native";
import TabBarIcon from "../components/TabBarIcon";
import TabBarImage from "../components/TabBarImage";
import CategoriesScreen from "../screens/CategoriesScreen";
import CategoriesMenu from "../screens/CategoriesMenu";
import MyStack from "../screens/MyStack";
import { isIphoneX } from "../constants/Device";

import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = "Home";

export default function BottomTabNavigator({ navigation, route }) {
  // Set the header title on the parent stack navigator depending on the
  // currently active tab. Learn more in the documentation:
  // https://reactnavigation.org/docs/en/screen-options-resolution.html
  //navigation.setOptions({ });

  return (
    <BottomTab.Navigator
      tabBarOptions={{
        style: { backgroundColor: "black", height: isIphoneX() ? 100 : 70 },
        activeTintColor: "white",
        labelStyle: {
          marginTop: -10
        }
      }}
      initialRouteName={INITIAL_ROUTE_NAME}
    >
      <BottomTab.Screen
        name="Produtos"
        component={MyStack}
        options={{
          title: "",
          tabBarIcon: ({ focused }) => (
            //<TabBarIcon focused={focused} name="md-timer" />
            <TabBarImage focused={focused} name="Produtos" />
          )
        }}
      />
      <BottomTab.Screen
        name="Lista"
        component={CategoriesScreen}
        options={{
          title: "",
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="Lista" />
            //<TabBarImage focused={focused} name="Lista" />
          )
        }}
      />
      <BottomTab.Screen
        name="Pedidos"
        component={CategoriesScreen}
        options={{
          title: "",
          tabBarIcon: ({ focused }) => (
            //<TabBarIcon focused={focused} name="md-timer" />
            <TabBarImage focused={focused} name="Pedidos" />
          )
        }}
      />
      <BottomTab.Screen
        name="Ofertas"
        component={CategoriesScreen}
        options={{
          title: "",
          tabBarIcon: ({ focused }) => (
            //<TabBarIcon focused={focused} name="md-timer" />
            <TabBarImage focused={focused} name="Ofertas" />
          )
        }}
      />
      <BottomTab.Screen
        navigationOptions={{ tabBarVisible: false }}
        name="Jornal"
        component={CategoriesMenu}
        options={{
          title: "",
          tabBarIcon: ({ focused }) => (
            <TabBarImage focused={focused} name="Jornal" />
          )
        }}
      />
    </BottomTab.Navigator>
  );
}

function getHeaderTitle(route) {
  const routeName =
    route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;

  switch (routeName) {
    case "Home":
      return "How to get started";
    case "Links":
      return "Links to learn more";
  }
}
