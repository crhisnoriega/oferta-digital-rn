import React, { Component } from "react";
import {
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  Dimensions,
  StatusBar,
  Image
} from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { MaterialIcons, FontAwesome } from "@expo/vector-icons";
import { DrawerActions } from "@react-navigation/native";
import { Badge } from "react-native-elements";
import { Entypo } from "@expo/vector-icons";

//const BadgedIcon = withBadge(2)(FontAwesome);

const bar_height = 80;
const bar_margin_top = 50;

class MenuItemIcon extends Component {
  render() {
    if (this.props.name == "Produtos") {
      marginLeft = 15;
      icon = require("../assets/icons/menu_new_podutos.png");
    }

    if (this.props.name == "Lista") {
      icon = require("../assets/icons/menu_new_lista.png");
    }

    if (this.props.name == "Ofertas") {
      icon = require("../assets/icons/menu_ofertas_diarias.png");
    }

    if (this.props.name == "Pedidos") {
      icon = require("../assets/icons/menu_pedidos.png");
    }

    if (this.props.name == "Jornal") {
      icon = require("../assets/icons/menu_new_jornal.png");
    }

    return (
      <TouchableOpacity
        onPress={() => {
          console.log(this.props.navigation);
        }}
      >
        <View
          style={{
            flexDirection: "row",
            marginLeft: 20,
            marginRight: 40,
            alignItems: "center"
          }}
        >
          <Entypo
            name="list"
            size={30}
            style={{
              width: 30,
              height: 30,
              color: "white"
            }}
          />

          <Text
            onChangeText={senha => this.setState({ senha })}
            underlineColorAndroid="transparent"
            style={{
              alignItems: "center",
              height: 30,
              backgroundColor: "transparent",
              marginLeft: 20,
              marginTop: 10,
              fontSize: 19,
              color: "white"
            }}
          >
            {this.props.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default MenuItemIcon;

const styles = StyleSheet.create({
  scene: {
    flex: 1
  }
});
