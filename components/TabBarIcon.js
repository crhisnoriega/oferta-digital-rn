import { Entypo } from "@expo/vector-icons";
import * as React from "react";
import { Image, Text, View } from "react-native";

import Colors from "../constants/Colors";

export default function TabBarIcon(props) {
  return (
    <View
      style={{
        width: 60,
        height: 60,
        justifyContent: "center"
      }}
    >
      <Entypo
        name="list"
        size={30}
        style={{
          marginTop: -5,
          color: props.focused ? "white" : "gray",
          marginRight: 10
        }}
      />

      <Text
        style={{
          marginLeft: 5,
          marginTop: -8,
          color: props.focused ? "white" : "gray",
          fontSize: 12
        }}
      >
        {props.name}
      </Text>
    </View>
  );
}
