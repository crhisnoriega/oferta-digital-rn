import { Ionicons } from "@expo/vector-icons";
import * as React from "react";
import { Image, Text, View } from "react-native";

import Colors from "../constants/Colors";

export default function TabBarImage(props) {
  var icon = null;
  var icon_size = 20;
  var side = 60;
  var marginLeft = 10;
  if (props.name == "Produtos") {
    marginLeft = 15;
    icon = require("../assets/icons/menu_new_podutos.png");
  }

  if (props.name == "Lista") {    
    icon = require("../assets/icons/menu_new_lista.png");
  }

  if (props.name == "Ofertas") {
    icon = require("../assets/icons/menu_ofertas_diarias.png");
  }

  if (props.name == "Pedidos") {
    icon = require("../assets/icons/menu_pedidos.png");
  }

  if (props.name == "Jornal") {
    icon = require("../assets/icons/menu_new_jornal.png");
  }

  return (
    <View
      style={{
        width: side,
        height: side,
        justifyContent: "center"
      }}
    >
      <Image
        style={{
          justifyContent: "center",
          width: icon_size,
          marginLeft: marginLeft,
          height: icon_size,
          tintColor: props.focused ? "white" : "gray"
        }}
        source={icon}
        size={icon_size}
      />
      <Text
        style={{
          justifyContent: "center",
          alignItems: "center",
          marginLeft: 5,
          color: props.focused ? "white" : "gray",
          fontSize: 12
        }}
      >
        {props.name}
      </Text>
    </View>
  );
}
