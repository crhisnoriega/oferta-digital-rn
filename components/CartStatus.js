import React, { Component } from "react";
import {
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  Dimensions,
  StatusBar,
  TextInput,
  Platform
} from "react-native";

import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { MaterialIcons, FontAwesome } from "@expo/vector-icons";
import { DrawerActions } from "@react-navigation/native";
import { Badge } from "react-native-elements";
import { SearchBar } from "react-native-elements";
import { AsyncStorage } from "react-native";

import FirebaseStore from "../infra/FirebaseStore";

//const BadgedIcon = withBadge(2)(FontAwesome);
TextInput.defaultProps.selectionColor = "white";

const bar_height = 90;
const bar_margin_top = 50;

class CartStatus extends Component {
  state = {
    search: "",
    quantity: 0,
    cart: []
  };

  constructor() {
    super();
    // console.log(firebase);
    this.firebase = new FirebaseStore();
  }

  async componentDidMount() {
    this.username = await AsyncStorage.getItem("username");
    this.firebase.retrieveFirebase("users/" + this.username + "/cart", data => {
      this.setState({
        cart: data,
        quantity: Object.keys(data).length
      });
    });
  }
  updateQ = (quantity, item) => {};

  render() {
    return (
      <View
        style={{
          ...this.props.style,
          flexDirection: "row",
          height: bar_height,
          backgroundColor: "black"
        }}
      >
        <TouchableOpacity
          style={{ marginTop: bar_margin_top, margin: 10 }}
          onPress={() =>
            this.props.navigation.dispatch(DrawerActions.openDrawer())
          }
        >
          <MaterialIcons name="menu" size={30} style={{ color: "white" }} />
        </TouchableOpacity>

        {this.props.showsearch ? (
          <SearchBar
            containerStyle={{
              marginTop: bar_margin_top - 10,
              width: "75%",
              backgroundColor: "black"
            }}
            inputContainerStyle={{
              backgroundColor: "black",
              selectionColor: "black"
            }}
            placeholder="Produto..."
            onChangeText={search => {
              this.setState({ search });
            }}
            value={this.state.search}
          />
        ) : (
          <View />
        )}

        <TouchableOpacity
          style={{
            marginLeft: "auto"
          }}
          onPress={() => {
            this.props.navigation.navigate("CarListScreen");
          }}
        >
          <FontAwesome
            name="shopping-cart"
            size={30}
            style={{
              color: "red",
              marginTop: 50,
              marginRight: 10
            }}
          />

          <Badge
            badgeStyle={{ backgroundColor: "white", color: "black" }}
            textStyle={{ color: "red" }}
            value={this.state.quantity}
            containerStyle={{
              position: "absolute",
              top: 45,
              right: 5,
              color: "black"
            }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default CartStatus;

const styles = StyleSheet.create({
  scene: {
    flex: 1
  }
});
