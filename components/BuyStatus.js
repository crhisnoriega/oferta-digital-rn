import React, { Component } from "react";
import {
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  Dimensions,
  StatusBar,
  TextInput,
  Platform,
  Image
} from "react-native";

import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { MaterialIcons, FontAwesome, Entypo } from "@expo/vector-icons";
import { DrawerActions } from "@react-navigation/native";
import { Badge } from "react-native-elements";
import { SearchBar } from "react-native-elements";
import firebase from "../infra/Firebase";
import { AsyncStorage } from "react-native";

//const BadgedIcon = withBadge(2)(FontAwesome);
TextInput.defaultProps.selectionColor = "white";

const bar_height = 90;
const bar_margin_top = 50;

class BuyStatus extends Component {
  state = {
    search: "",
    quantity: 0,
    cart: []
  };

  constructor() {
    super();
    console.log(firebase);
  }

  async componentDidMount() {
    this.username = await AsyncStorage.getItem("username");
    console.log(this.username);
    firebase
      .database()
      .ref("users/" + this.username + "/cart")
      .on("value", snapshot => {
        if (snapshot) {
          var cart_val = snapshot.val();
          this.setState({
            cart: cart_val,
            quantity: Object.keys(cart_val).length
          });
        }
      });
  }
  updateQ = (quantity, item) => {
    firebase
      .database()
      .ref("user_" + this.username)
      .set([{ ...item }]);
    this.setState({ quantity });
  };

  render() {
    return (
      <View
        style={{
          ...this.props.style,
          flexDirection: "row",
          height: bar_height,
          backgroundColor: "black"
        }}
      >
        <TouchableOpacity
          style={{ marginTop: bar_margin_top, margin: 10 }}
          onPress={() =>
            this.props.navigation.dispatch(DrawerActions.openDrawer())
          }
        >
          <MaterialIcons name="menu" size={30} style={{ color: "white" }} />
        </TouchableOpacity>

        {this.props.showsearch ? (
          <SearchBar
            containerStyle={{
              marginTop: bar_margin_top - 10,
              width: "75%",
              backgroundColor: "black"
            }}
            inputContainerStyle={{
              backgroundColor: "black",
              selectionColor: "black"
            }}
            placeholder="Produto..."
            onChangeText={search => {
              this.setState({ search });
            }}
            value={this.state.search}
          />
        ) : (
          <View />
        )}

        <View
          style={{
            flexDirection: "row",
            marginLeft: "auto"
          }}
        >
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate("CarListScreen");
            }}
          >
            <Entypo
              name="list"
              size={30}
              style={{
                color: "blue",
                marginTop: 50,
                marginRight: 10
              }}
            />
          </TouchableOpacity>

          
        </View>
      </View>
    );
  }
}

export default BuyStatus;

const styles = StyleSheet.create({
  scene: {
    flex: 1
  }
});
