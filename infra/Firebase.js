import * as firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyAXHQpbGz1bLp4MM7zuAxVE4PdAZ7SiTbY",
  authDomain: "oferta-digital-rn.firebaseapp.com",
  databaseURL: "https://oferta-digital-rn.firebaseio.com",
  projectId: "oferta-digital-rn",
  storageBucket: "oferta-digital-rn.appspot.com",
  messagingSenderId: "1077927125709",
  appId: "1:1077927125709:web:3a1c289d8bb75e4c063f84"
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
  var connectedRef = firebase.database().ref(".info/connected");
  connectedRef.on("value", function(snap) {
    if (snap.val() === true) {
      console.log("connected");
    } else {
      console.log("not connected");
    }
  });
}

export default firebase;
